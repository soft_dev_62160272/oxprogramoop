/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Thanyarak Namwong
 */
package com.thanya.oxprogramoop;

import java.util.Scanner;

public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();

    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");

    }

    public void input() {
        while (true) {

            System.out.println("Plese input Row Col: ");

            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not emtry!!!");
        }

    }
    public void newGame(){
        table = new Table(playerX,playerO);
    }
    

    public void run() {
        this.showWelcome();
        while (true) {

            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null) {
                    this.showTable();
                    System.out.println("Draw!!");

                } else {
                    this.showTable();
                    System.out.println("Player "+table.getWinner().getName() + " Win!!");
                }
                break;
            }
            table.switchPlayer();
        }

    }

}
